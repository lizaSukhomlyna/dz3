package com.app.controller;

import com.app.model.Person;
import com.app.model.Product;
import com.app.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/person")
public class PersonController {

    @Autowired
    private PersonService personService;

   @PostMapping("/create")
   public String createPerson(@RequestBody Person person){
        return personService.create(person);
    }


    @PostMapping("/addProduct")
    public String addProduct(@RequestBody Product product){
        return personService.putProductOnYourCardByPersonId(1,product.getId(),product);
    }
    @GetMapping("/get")
    public String getPerson(@RequestParam Integer id){
        return personService.getPersonById(id).toString();
    }

    @GetMapping("/getCards")
    public String getCards(@RequestParam Integer id){

       return personService.getCardsByPersonId(id).toString();
    }

    @GetMapping("/getCost")
    public String getCost(@RequestParam Integer id){
        return personService.getGeneralCostByPersonId(id).toString();
    }

    @GetMapping("/writeToFile")
    public String writeToFile(){
        personService.writeDataToFile();
        return "OK";
    }



}
