package com.app.data;

import com.app.model.Card;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Getter
@Setter
public class CardCollection {
    public List<Card> cards=new ArrayList<>();

    @Override
    public String toString() {
        return "CardCollection{" +
                "cards=" + cards +
                '}';
    }
}
