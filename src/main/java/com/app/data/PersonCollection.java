package com.app.data;

import com.app.model.Person;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
@Getter
@Setter
public class PersonCollection {
    public  Set<Person> persons=new HashSet<>();

    @Override
    public String toString() {
        return "PersonCollection{" +
                "persons=" + persons +
                '}';
    }
}
