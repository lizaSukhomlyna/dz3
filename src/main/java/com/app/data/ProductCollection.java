package com.app.data;

import com.app.model.Product;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Getter
@Setter
public class ProductCollection {
    public Map<Integer,Product> products=new HashMap<>();

    @Override
    public String toString() {
        return "ProductCollection{" +
                "products=" + products +
                '}';
    }
}
