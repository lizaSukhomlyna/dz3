package com.app.model;

import com.app.data.ProductCollection;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class Card {
    private Integer idCard=1;
    private BigDecimal cost=null;
    public ProductCollection productCollection= new ProductCollection();

    public BigDecimal getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return "Card{" +
                "cost=" + cost +
                ", productCollection=" + productCollection +
                '}';
    }

   public void calculateCost(){
       cost=productCollection.products.values().stream().map(Product::getCost).reduce(BigDecimal.ZERO, BigDecimal::add);
   }
}
