package com.app.model;

import com.app.data.CardCollection;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class Person {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private Map<Integer,CardCollection> cardOfPerson=new HashMap<>();


    public void setCardOfPerson(Integer id,CardCollection cards) {
        this.cardOfPerson.put(id,cards);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", cardOfPerson=" + cardOfPerson +
                '}';
    }
}
