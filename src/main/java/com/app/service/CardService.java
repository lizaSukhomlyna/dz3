package com.app.service;

import com.app.model.Card;
import com.app.model.Product;

public interface CardService {
    public Integer addCard(Integer idPerson, Card card);

}
