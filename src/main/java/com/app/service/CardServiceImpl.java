package com.app.service;

import com.app.data.CardCollection;
import com.app.data.PersonCollection;
import com.app.model.Card;
import com.app.model.Product;
import org.springframework.beans.factory.annotation.Autowired;

public class CardServiceImpl implements CardService {

    @Autowired
    CardCollection cardCollection;


    public Integer addCard(Integer idPerson, Card card) {
        cardCollection.cards.add(card);
        return card.getIdCard();
    }

}
