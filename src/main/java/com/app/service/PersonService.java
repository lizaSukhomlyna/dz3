package com.app.service;

import com.app.data.CardCollection;
import com.app.model.Person;
import com.app.model.Product;

import java.math.BigDecimal;

public interface PersonService {
    public String create(Person person);
    public Person getPersonById(Integer id);

    public CardCollection getCardsByPersonId(Integer id);

    public String putProductOnYourCardByPersonId(Integer idPerson, Integer idProduct, Product product);

    public BigDecimal getGeneralCostByPersonId(Integer id);
    public void writeDataToFile();
}
