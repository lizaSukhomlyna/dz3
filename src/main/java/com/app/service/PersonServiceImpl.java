package com.app.service;

import com.app.data.CardCollection;
import com.app.data.PersonCollection;
import com.app.model.Card;
import com.app.model.Person;
import com.app.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;
@Service
public class PersonServiceImpl implements PersonService{

    @Autowired
    PersonCollection personCollection;

    @Autowired
    WriteDataService writeService;

    @Override
    public String create(Person person) {
        Person newPerson = new Person();
        Card newCard=new Card();
        newCard.setIdCard(1);
        newCard.setCost(BigDecimal.valueOf(0));
        CardCollection newCardCollection=new CardCollection();
        newCardCollection.cards.add(newCard);
        newPerson.setId(person.getId());
        newPerson.setFirstName(person.getFirstName());
        newPerson.setLastName(person.getLastName());
        newPerson.setEmail(person.getEmail());
        newPerson.setCardOfPerson(person.getId(),newCardCollection);
        personCollection.persons.add(newPerson);
        return newPerson.getFirstName();
    }

    @Override
    public Person getPersonById(Integer id) {
        return personCollection.getPersons().stream().filter(el-> Objects.equals(el.getId(), id)).findAny().get();
    }

    @Override
    public CardCollection getCardsByPersonId(Integer id) {
        return personCollection.getPersons().stream()
                .filter(el-> Objects.equals(el.getId(), id))
                .map(Person::getCardOfPerson)
                .findFirst().get().get(1);
    }
    @Override
    public String putProductOnYourCardByPersonId(Integer idPerson, Integer idProduct, Product product){
       personCollection.getPersons().stream()
                .filter(el-> Objects.equals(el.getId(), idPerson))
                .map(Person::getCardOfPerson)
               .forEach(el->el.get(1).cards.get(0).productCollection.products.put(product.getId(),product));
        personCollection.getPersons().stream()
                .filter(el-> Objects.equals(el.getId(), idPerson))
                .map(Person::getCardOfPerson)
                .forEach(el->el.get(1).cards.get(0).calculateCost());
       return product.getName();

    }
    @Override
    public BigDecimal getGeneralCostByPersonId(Integer id) {
        return personCollection.getPersons().stream()
                .filter(el-> Objects.equals(el.getId(), id))
                .map(Person::getCardOfPerson)
                .map(el->el.get(1).cards.get(0).getCost())
                .findFirst().get();
    }

    @Override
    public void writeDataToFile() {
        writeService.writeData(personCollection);
    }
}
