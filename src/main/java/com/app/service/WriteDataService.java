package com.app.service;

import com.app.data.PersonCollection;

public interface WriteDataService {
    public void writeData(PersonCollection personCollection);
}
