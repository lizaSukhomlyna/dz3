package com.app.service;

import com.app.data.PersonCollection;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class WriteDataServiceToCSV implements WriteDataService {
    @Override
    public void writeData(PersonCollection personCollection) {
            File csvOutputFile = new File("src/main/resources/data_person.csv");
            try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
                personCollection.getPersons().stream()
                        .map(el->convertToCSV(el.toString()))
                        .forEach(pw::println);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }


    private String convertToCSV(String data) {
        return Stream.of(data)
                .map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    public String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }
}
